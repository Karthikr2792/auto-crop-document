function [sx, sy, sWidth, sHeight] = auto_crop ( f )
g = f;
[rin, cin] = size(rgb2gray(g));
f = imresize(f, [600 600]);
%getting size of the input image
Ro = size(f,1);
Co = size(f,2);

enhF = rgb2gray(f);
enhF = imfill(enhF);
enhF = enhF*0.2;
enhF = rangefilt(enhF);
edges = edge(enhF, 'Canny', 0.1, 2);
edges = bwareaopen(edges, 400);

edges = imdilate(edges, strel('diamond', 2));
edges = imfill(edges, 'holes');
 if(edges(Ro/2,Co/2) == 0)
      edges = imcomplement(edges);
 end;
 figure, imshow(edges);
gradoperatorCol = [true true];
gradoperatorRow = gradoperatorCol';
leftIndex = 50;
rightIndex = 550;
topIndex = 50;
bottomIndex = 550;
for i=floor(Ro/2):-1:50
    var1 = edges(i:-1:i-1,floor(Co/2));
    top = gradoperatorRow.*var1;
    if xor(top(1), top(2))
        topIndex = i;
        break;
    end
end
for i=floor(Ro/2): 550
    var1 = edges(i:i+1,floor(Co/2));
    top = gradoperatorRow.*var1;
    if xor(top(1), top(2))
        bottomIndex = i;
        break;
    end
end
for i=floor(Co/2):-1:50
    var1 = edges(floor(Ro/2),i:-1:i-1);
    top = gradoperatorCol.*var1;
    if xor(top(1), top(2))
        leftIndex = i;
        break;
    end
end
for i=floor(Ro/2):550
    var1 = edges(floor(Ro/2), i:i+1);
    top = gradoperatorCol.*var1;
    if xor(top(1), top(2))
        rightIndex = i;
        break;
    end
end
topIndex = topIndex*rin/600;
bottomIndex = bottomIndex*rin/600;
leftIndex = leftIndex*cin/600;
rightIndex = rightIndex*cin/600;

sHeight = bottomIndex - topIndex;
sWidth = rightIndex - leftIndex;
sx = leftIndex;
sy = topIndex;
            

