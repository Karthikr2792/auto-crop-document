You will find

auto_crop.m: the main function for cropping.  Returns x,y locations of top left corner of document and width and height of bounding box  A sample "dumb" version is included.

grade_auto_crop.m: driver for testing and grading
input*.png: input for cropping

*.csv: a CSV file that contains the location to crop (x,y,width,height) where top left corner of object is at (x,y)

Works with Matlab R2015a. One of the program lines uses a different syntax under Matlab R2016a and hence might crash
